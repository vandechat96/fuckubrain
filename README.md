# FuckUBrain

IDE and interpreter for brainfuck written in JS with [ElectronJS](https://www.electronjs.org/) 

This project was previously hosted by [Loic-Delbarre](https://github.com/Loic-Delbarre) on GitHub

##Install packages

`
npm isntall
`

##Run 

`
npm start
`

Thanks to [Loic-Delbarre](https://github.com/Loic-Delbarre) and [Gilles59](https://github.com/Gilles59) for helping in this project

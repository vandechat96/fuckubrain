// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// No Node.js APIs are available in this process because
// `nodeIntegration` is turned off. Use `preload.js` to
// selectively enable features needed in the rendering
// process.
//WIP ZONE TEST

function openNav() {
    document.getElementById("mySidenav").style.height = "260px";
}

function closeNav() {
    document.getElementById("mySidenav").style.height = "0";
}

function openNavd() {
    document.getElementById("mySidenavd").style.width = "250px";
  }
  
function closeNavd() {
    document.getElementById("mySidenavd").style.width = "0";
}


function on_input(ev, numberZone, textZone) {
    let lines = count_lines(ev.target.value)
    let txt = ''
    for (let i = 1; i <= lines; i++) {
        txt += i + '\n'
    }
    numberZone.lines = textZone.lines
    numberZone.value = txt
    scroll_sync(numberZone, textZone)
}

function count_lines(string) {
    return string.split(/\n/).length
}

function on_scrolled(ev, numberZone, textZone) {
    scroll_sync(numberZone, textZone)

}

function scroll_sync(numberZone, textZone) {

    numberZone.scrollTop = textZone.scrollTop

}

function start(step) {
    const {interpreter} = require('./Engine')
    if (interpreter.finished) {
        let textdiv = document.getElementById('textdiv')

        textdiv.hidden = true
        document.getElementById('preview').hidden = false



        const script = (document.getElementById('textzone').value.match(/[+\-><\[\].,]+/g).join(""))
        interpreter.initialize(script)
    }
    if (step) return
    interpreter.changeDelay(document.getElementById('delay').value)
    if (interpreter.paused) {
        interpreter.start()
    } else {
        interpreter.pause()
    }
}

function visualize(interpreter) {
    let tape_ = document.getElementsByClassName('tape').item(0)
    if (tape_.children.length < interpreter.tape.length) {
        let nbr = interpreter.tape.length - tape_.children.length
        while (nbr--) {
            tape_.innerHTML += '<li>0</li>'
        }

    }
    for (let i = 0; i < interpreter.tape.length; i++) {
        tape_.children[i].innerHTML = interpreter.tape[i] + ""
    }

    let cursor = document.getElementsByClassName('pointer').item(0)
    cursor.style.marginTop = 32 * interpreter.ptr + 'px'

    let txt = ''     // pour le curseur
    let i = -1
    while (i++ < interpreter.prgIndex - 1) {
        txt += interpreter.program[i]
    }
    txt += '<span class="caret">' + interpreter.program[i] + '</span>'
    while (i++ < interpreter.program.length - 1) {
        txt += interpreter.program[i]
    }
    document.getElementById('preview').innerHTML = txt
}

function reset_() {
    document.getElementById('textdiv').hidden = false
    document.getElementById('preview').hidden = true
}

function resetButton() {
    const {interpreter} = require('./Engine')
    interpreter.finish()
    reset_()
}

function changeDelay() {
    const {interpreter} = require('./Engine')
    interpreter.changeDelay(document.getElementById('delay').value)
}

function forward() {
    const {interpreter} = require('./Engine')
    if (interpreter.finished) {
        start(true)
    }
    if (interpreter.prgIndex < interpreter.program.length) {
        interpreter.next()
        interpreter.print_out()
    }
}

function backward() {
    const {interpreter} = require('./Engine')
    //todo : finir cas ]
    /*
    if (interpreter.paused && !interpreter.finished) {
        interpreter.back()
    }

     */
}
// All of the Node.js APIs are available in the preload process.
// It has the same sandbox as a Chrome extension.
window.addEventListener('DOMContentLoaded', () => {

    let numberZone = document.getElementById('numberzone')
    numberZone.innerText = "1"

    let textZone = document.getElementById('textzone')
    textZone.onscroll = (ev) => on_scrolled(ev, numberZone, textZone)
    textZone.oninput = (ev) => on_input(ev, numberZone, textZone)

    document.getElementById("openConsole").onclick = openNav
    document.getElementById("closeConsole").onclick = closeNav
    document.getElementById("openConsoled").onclick = openNavd
    document.getElementById("closeConsoled").onclick =  closeNavd

    document.getElementById("startButton").onclick = () => start()
    document.getElementById("delay").onchange = changeDelay
    document.getElementById("nextButton").onclick = forward
    document.getElementById("backButton").onclick = backward
    document.getElementById("reset").onclick = resetButton

    const replaceText = (selector, text) => {
        const element = document.getElementById(selector)
        if (element) element.innerText = text
    }

    for (const type of ['chrome', 'node', 'electron']) {
        replaceText(`${type}-version`, process.versions[type])
    }
})
